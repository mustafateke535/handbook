---

title: "CSM/CSE Webinar & Hands-On Labs Calendar"
---
# On this page



View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM/CSE-related handbook pages.

Watch previously recorded webinars on our [YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpczt4pRtyF147Uvn2bGGvq).

---
# Upcoming Events

We’d like to invite you to our free upcoming webinars and labs in the month of March 2024.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!

## March 2024

### AMER Time Zone Webinars & Labs

#### Security and Compliance
##### March 26th, 2024 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_H4LE0jLcTA-f9r4lRb7DmQ#/registration)

#### Hands-On Advanced GitLab CI Lab 
##### March 27th, 2023 at 9:00-11:00AM Pacific Time / 12:00-2:00PM Eastern Time

Join us for a hands-on lab where we will go into advanced GitLab CI capabilities that allow customers to simplify pipeline yml code and optimize the execution time of pipelines.

We will cover:
- Storing build resources in GitLab registries
- Managing artifacts, dependencies, and environment variables between jobs
- Running a job multiple times in parallel with different variables
- Triggering downstream pipelines

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_JER8cqk9T9uLPObVrb_-gA#/registration)

### EMEA Time Zone Webinars & Labs

#### Security and Compliance
##### March 26th, 2024 at 10:00-11:00AM UTC / 11:00AM-12:00PM CET

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_G5BXFRtvREiv2Smo_p2hmg#/registration)

#### Hands-On Advanced GitLab CI Lab 
##### March 27th, 2023 at 10:00AM-12:00PM UTC / 11:00AM-1:00PM CET

Join us for a hands-on lab where we will go into advanced GitLab CI capabilities that allow customers to simplify pipeline yml code and optimize the execution time of pipelines.

We will cover:
- Storing build resources in GitLab registries
- Managing artifacts, dependencies, and environment variables between jobs
- Running a job multiple times in parallel with different variables
- Triggering downstream pipelines

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_e_bF-NCjRHOJBXGREyWcbA#/registration)

Check back later for more webinars & labs! 


